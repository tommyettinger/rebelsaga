package net.myexperiments.rebelsaga.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import net.myexperiments.rebelsaga.OffenseSkill;
import au.com.bytecode.opencsv.CSVReader;
import com.esotericsoftware.kryo.*;
import com.esotericsoftware.kryo.serialize.MapSerializer;
public class Loader {

	// String[] fields;

	public Loader() throws IOException {
		// setWorld(world);
	}
	public static HashMap<String, OffenseSkill> writeOffenseSkills(String inputFilename, String outputFilename) throws IOException
	{
		Kryo kryo = new Kryo();
		kryo.register(OffenseSkill.class);
		kryo.register(HashMap.class, new MapSerializer(kryo));
		
		// ...
		//SomeClass someObject = new SomeClass(...);
		HashMap<String, OffenseSkill> loaded = loadOffenseSkills(inputFilename);
		
		ObjectBuffer buffer = new ObjectBuffer(kryo);
		buffer.writeObject(new FileOutputStream(outputFilename), loaded);

		//kryo.writeObject(buffer, loaded);
		// ...
		//HashMap<String, OffenseSkill> someObject = (HashMap<String, OffenseSkill>) buffer.readObject(new FileInputStream(outputFilename), HashMap.class);
		return loaded;
	}
	@SuppressWarnings("unchecked")
	public static HashMap<String, OffenseSkill> readOffenseSkills(String inputFilename) throws FileNotFoundException
	{

		Kryo kryo = new Kryo();
		kryo.register(OffenseSkill.class);
		kryo.register(HashMap.class, new MapSerializer(kryo));
		ObjectBuffer buffer = new ObjectBuffer(kryo);
		return (HashMap<String, OffenseSkill>) buffer.readObject(new FileInputStream(inputFilename), HashMap.class);
	}

	public static HashMap<String, OffenseSkill> loadOffenseSkills(String filename)
			throws IOException {
		HashMap<String, OffenseSkill> skillsLoaded = new HashMap<String, OffenseSkill>();
		CSVReader reader = new CSVReader(new FileReader(filename), '\t');

		//String[] fields = reader.readNext();
		List<String[]> table = reader.readAll();
		String[] data;
		Iterator<String[]> iter = table.iterator();
		iter.next();
		while (iter.hasNext()) {
			data = iter.next();
			/*
			 * HashMap<String, String> rowdata = new HashMap<String, String>();
			 * 
			 * for (int i = 0; i < data.length; i++) {
			 * 
			 * rowdata.put(fields[i], data[i]); }
			 */
			OffenseSkill skillData = new OffenseSkill();
			
			skillData.Skill = data[0];
			skillData.Mobility = Integer.parseInt(data[1]);
			skillData.Awareness = Integer.parseInt(data[2]);
			skillData.Presence = Integer.parseInt(data[3]);
			skillData.Intellect = Integer.parseInt(data[4]);
			skillData.Toughness = Integer.parseInt(data[5]);
			skillData.Versus_Mobility = Integer.parseInt(data[6]);
			skillData.Versus_Awareness = Integer.parseInt(data[7]);
			skillData.Versus_Presence = Integer.parseInt(data[8]);
			skillData.Versus_Intellect = Integer.parseInt(data[9]);
			skillData.Versus_Toughness = Integer.parseInt(data[10]);
			skillData.Bohan = Integer.parseInt(data[11]);
			skillData.Chott = Integer.parseInt(data[12]);
			skillData.Human = Integer.parseInt(data[13]);
			skillData.Jeyo = Integer.parseInt(data[14]);
			skillData.Kamoan = Integer.parseInt(data[15]);
			skillData.Mech = Integer.parseInt(data[16]);
			skillData.Nalamar = Integer.parseInt(data[17]);
			skillData.Radoni = Integer.parseInt(data[18]);
			skillData.Tilak = Integer.parseInt(data[19]);
			skillData.Woro = Integer.parseInt(data[20]);
			skillData.Assassin = Integer.parseInt(data[21]);
			skillData.Brawler = Integer.parseInt(data[22]);
			skillData.Commander = Integer.parseInt(data[23]);
			skillData.Disciple = Integer.parseInt(data[24]);
			skillData.Genius = Integer.parseInt(data[25]);
			skillData.Hunter = Integer.parseInt(data[26]);
			skillData.Mechanic = Integer.parseInt(data[27]);
			skillData.Noble = Integer.parseInt(data[28]);
			skillData.Oracle = Integer.parseInt(data[29]);
			skillData.Professional = Integer.parseInt(data[30]);
			skillData.Raider = Integer.parseInt(data[31]);
			skillData.Saboteur = Integer.parseInt(data[32]);
			skillData.Trickster = Integer.parseInt(data[33]);
			skillData.Vanguard = Integer.parseInt(data[34]);
			skillData.Warrior = Integer.parseInt(data[35]);
			skillData.Damage = Integer.parseInt(data[36]);
			skillData.Persistent = Integer.parseInt(data[37]);
			skillData.Exertion = Integer.parseInt(data[38]);
			skillData.Move = Integer.parseInt(data[39]);
			skillData.Jump = Integer.parseInt(data[40]);
			skillData.Push = Integer.parseInt(data[41]);
			skillData.Pull = Integer.parseInt(data[42]);
			skillData.Slide = Integer.parseInt(data[43]);
			skillData.Range = Integer.parseInt(data[44]);
			skillData.Area_Burst = Integer.parseInt(data[45]);
			skillData.Area_Blast = Integer.parseInt(data[46]);
			skillData.Status_Bleed = Integer.parseInt(data[47]);
			skillData.Status_Blind = Integer.parseInt(data[48]);
			skillData.Status_Break = Integer.parseInt(data[49]);
			skillData.Status_Confused = Integer.parseInt(data[50]);
			skillData.Status_Dazed = Integer.parseInt(data[51]);
			skillData.Status_Disarmed = Integer.parseInt(data[52]);
			skillData.Status_Drained = Integer.parseInt(data[53]);
			skillData.Status_Fatigued = Integer.parseInt(data[54]);
			skillData.Status_Fear = Integer.parseInt(data[55]);
			skillData.Status_Pain = Integer.parseInt(data[56]);
			skillData.Status_Pin = Integer.parseInt(data[57]);
			skillData.Status_Poisoned = Integer.parseInt(data[58]);
			skillData.Status_Prone = Integer.parseInt(data[59]);
			skillData.Status_Sickened = Integer.parseInt(data[60]);
			skillData.Status_Slowed = Integer.parseInt(data[61]);
			skillData.Element = data[62];
			skillData.Requirements = data[63];

			skillsLoaded.put(data[0], skillData);
		}

		return skillsLoaded;

	}

}
