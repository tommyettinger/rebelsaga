package net.myexperiments.rebelsaga;

public class OffenseSkill extends Skill {
	public int Mobility = 0, Awareness = 0, Presence = 0, Intellect = 0, Toughness = 0, Versus_Mobility = 0, 
			Versus_Awareness = 0, Versus_Presence = 0, Versus_Intellect = 0, 
			Versus_Toughness = 0, Damage = 0, Persistent = 0, 
			Exertion = 0, Move = 0, Jump = 0, Push = 0, Pull = 0, Slide = 0, Range = 0, Area_Burst = 0, 
			Area_Blast = 0, Status_Bleed = 0, Status_Blind = 0, Status_Break = 0, 
			Status_Confused = 0, Status_Dazed = 0, Status_Disarmed = 0, Status_Drained = 0, 
			Status_Fatigued = 0, Status_Fear = 0, Status_Pain = 0, Status_Pin = 0, 
			Status_Poisoned = 0, Status_Prone = 0, Status_Sickened = 0, Status_Slowed = 0;
	public String Skill, Element, Requirements;
	public OffenseSkill()
	{
		
	}
	
}
