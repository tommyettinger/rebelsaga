package net.myexperiments.rebelsaga;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import net.myexperiments.rebelsaga.util.Loader;

//import net.myexperiments.gos.World;

public class RebelSaga {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
	/*
		World world = new World();
		world.setCharacterdata("text\\charactersdata.txt");
		world.setRoomdata("text\\roomdata.txt");
		world.setSpeciesdata("text\\RebelSagaSpecies.txt");
		world.setClassesdata("text\\RebelSagaClasses.txt");
		world.setWorldObjectdata("text\\WorldObjects.txt");
		world.InitWorld();
		*/
/**
 * 
 * 		Get the Hashed Characters file 
 * 
 */
		 /*
		 HashMap<String, HashMap<String, String>> characterStuff = world.getCharacters().getObjectsloaded();
		 HashMap<String, HashMap<String, String>> speciesStuff = world.getSpecies().getObjectsloaded();
		 
		String rastusSpecies  = characterStuff.get("Rastus").get("Species");
		System.out.println("Rastus is a "+ rastusSpecies);
		HashMap<String, String> speciesTableEntry = speciesStuff.get(rastusSpecies);
		System.out.print("Species " + rastusSpecies + " have " +speciesTableEntry.toString());

		
		
		world.Start();
		*/
		System.out.println("Loading...");

//		HashMap<String, OffenseSkill> skills = Loader.writeOffenseSkills("text" + File.separator + "OffenseSkills.txt", "data" + File.separator + "OffenseSkills.kryo");
		HashMap<String, OffenseSkill> skills;
		try
		{
			skills = Loader.readOffenseSkills("data" + File.separator + "OffenseSkills.kryo");
		} catch(FileNotFoundException fnfe)
		{
			System.out.println("Failure reading in offense skills!");
			fnfe.printStackTrace();
			return;
		}
/*
		for(int i = 0; i < 50000; i++)
			skills = Loader.readOffenseSkills("data" + File.separator + "OffenseSkills.kryo");
*/
		OffenseSkill grab = skills.get("Grapple");
		System.out.println(grab.Requirements);
	}

}
